% coating.m
% [Ostertagova14] Methodology and Application of the Kruskal-Wallis Test.
% We are interested in the effect of conductivity regarding five types of coating.
% Using a significance level of alpha = 0.05, is there a difference due to the coating type?

% First approach
% Each column represents a different coating type
X = [143 150 134 129 147;
     141 149 133 127 148;
     150 137 132 132 144;
     146 134 127 129 142;
     145 152 128 130 143];

showplot = 'on';
[p, tbl, stats] = kruskalwallis(X, [], showplot);


% Second approach (each observation has a label)
% observations
X = [143 150 134 129 147 ...
     141 149 133 127 148 ...
     150 137 132 132 144 ...
     146 134 127 129 142 ...
     145 152 128 130 143];

% labels
lbls = {'type 1' 'type 2' 'type 3' 'type 4' 'type 5' ...
        'type 1' 'type 2' 'type 3' 'type 4' 'type 5' ...
        'type 1' 'type 2' 'type 3' 'type 4' 'type 5' ...
        'type 1' 'type 2' 'type 3' 'type 4' 'type 5' ...
        'type 1' 'type 2' 'type 3' 'type 4' 'type 5'};

%showplot = 'on';
%[p, tbl, stats] = kruskalwallis(X, lbls, showplot);

% compute the critical point of the chi-square distribution X_{0.95, 4}
cp = chi2inv(0.95, 4);

% display info
fprintf("p-value:         %.4f\n", p);
fprintf("H*-statistic:    %.4f\n", tbl{2,5});
fprintf("critical point:  %.4f\n", cp);

%figure;
%c = multcompare(stats, 'alpha', 0.05, 'ctype', 'bonferroni', 'Display', 'on');



