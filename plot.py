# plot.py
from __future__ import print_function
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
import matplotlib.patches as patches
from numpy.random import RandomState

def _load_rcparams(figsize=None, for_paper=False):
    """
    Load a custom rcParams dictionary
    """
    
    rcParams['axes.titlesize']  = 14            # title
    rcParams['axes.labelsize']  = 12            # $f_i$ labels
    rcParams['xtick.color']     = "#474747"     # ticks gray color
    rcParams['ytick.color']     = "#474747"     # ticks gray color
    rcParams['xtick.labelsize'] = 12            # ticks size
    rcParams['ytick.labelsize'] = 10            # ticks size
    rcParams['legend.fontsize'] = 12            # legend
    rcParams['legend.fontsize'] = 12            # legend

    if isinstance(figsize, tuple):
        rcParams['figure.figsize'] = figsize

def boxplot(data, labels, tab_summary=None, control="", use_jitter=True, rotation="horizontal", ylabel="", save=True, ylims=(-0.2, 1.2)):
        
    rand = RandomState(42)
    
    # split columns
    n_rows, n_cols = data.shape
    cols = [data[:, i] for i in range(n_cols)]
    
    _load_rcparams((9, 5))
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    bp = ax.boxplot(cols, patch_artist=True)
    
    # customize boxplot
    # http://blog.bharatbhole.com/creating-boxplots-with-matplotlib/
    for bi, box in enumerate(bp["boxes"]):
        box.set(color="#262626", linewidth=1)
        box.set(facecolor="#ffffff")
        
    for whisker in bp["whiskers"]:
        whisker.set(color="#262626", linewidth=1, ls="-")
    
    for cap in bp["caps"]:
        cap.set(color="#262626", linewidth=1)
        
    for bmed in bp["medians"]:
        bmed.set(color="#A91458", linewidth=1)
    
    #import ipdb; ipdb.set_trace()
    for flier in bp["fliers"]:
        flier.set(marker="o", ms=4, markerfacecolor="#c0c0c0")

    # plot points around boxplots
    for row_id, row_points in enumerate(data):
        
        y = row_points                          # eg [0.90, 0.91, ..., 0.99] igd values
        x = np.ones((len(y), )) * (row_id+1)    # eg [   1,    1, ...,    1] position on x axis
        
        # add jitter mean:0, sd:0.05
        # https://stackoverflow.com/questions/29779079/adding-a-scatter-of-points-to-a-boxplot-using-matplotlib
        if use_jitter:
            jitter = rand.normal(0, 0.05, size=len(y))
            x = x + jitter
        
            ax.scatter(x, y, facecolor="#74CE74", edgecolor="#262626", linewidth=0.15, s=10)

    # setup xticks
    ax.set_xticks([ i for i in range(1, n_cols+1)])             # 1, 2, ...
    ax.set_xticklabels(labels, rotation=rotation)     # 1: "WS", 2: "TE", ...


    if not tab_summary is None:
        
        if control:
    
            col_labels = []
            
            for i, method in enumerate(labels):
                
                new_label = ""
                key = "%s-%s" % (control, method)
                
                if key in tab_summary:
                    new_label = "%s (%s)" % (method, tab_summary[key])
                else:
                    new_label = method
                    
                col_labels.append(new_label)
                
            # setup xticks
            ax.set_xticks([ i for i in range(1, n_cols+1)])
            ax.set_xticklabels(col_labels, rotation=rotation)
            
        else:
            print("Use control and tab_summary")



    # set y label
    ax.set_ylabel(ylabel)
    
    # set lims
    ax.set_ylim(ylims[0], ylims[1])

    # setup margins
    plt.subplots_adjust(left=0.1, 
                        right=0.95, 
                        top=0.9, 
                        bottom=0.1, 
                        hspace=0.45,
                        wspace=0.1)
    
    # save plot
    if save:
        output = "boxplot_py.png"
        fig.savefig(output, dpi=300)
        print(output)
            
    plt.show()


if __name__ == "__main__":
    
    short_names = {
        "moead_norm_v1-ws-norm-true": "ws",
        "moead_norm_v1-te-norm-true": "te",
        "moead_norm_v1-asf-norm-true": "asf",
        "moead_norm_v1-pbi-norm-true": "pbi",
        "moead_norm_v1-ipbi-norm-true": "ipbi",
        "moead_norm_v1-vads-norm-true" : "vads",
        "mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true": "mss",
    }
    
    # we can also use pandas
    # this way, we can get the name of each column
    filename = "hv_dtlz1_m_objs_3_baselines_case_a.csv"
    df = pd.read_csv(filename, sep=";")
    X = df.values
    cols = df.columns
    cols = [short_names[item.replace("# ", "")] for item in cols]
    
    tab_summary = {
        'mss-asf': '+',
        'mss-ipbi': '+',
        'mss-pbi': '+',
        'mss-te': '+',
        'mss-vads': '+',
        'mss-ws': '+'}
    
    # optional: boxplot
    # create a boxplot for each column in X
    boxplot(X, cols, tab_summary, control="mss", ylabel="Hypervolume", use_jitter=False)
    
