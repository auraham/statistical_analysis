# coating.r
# [Ostertagova14] Methodology and Application of the Kruskal-Wallis Test.
# We are interested in the effect of conductivity regarding five types of coating.
# Using a significance level of alpha = 0.05, is there a difference due to the coating type?
# 
# run
# source('coating.r')

require(stats)
require(PMCMR)

# observations
obs <- c(143, 150, 134, 129, 147,
        141, 149, 133, 127, 148,
        150, 137, 132, 132, 144,
        146, 134, 127, 129, 142,
        145, 152, 128, 130, 143)

# labels
lbls <- c("type 1", "type 2", "type 3", "type 4", "type 5",
        "type 1", "type 2", "type 3", "type 4", "type 5",
        "type 1", "type 2", "type 3", "type 4", "type 5",
        "type 1", "type 2", "type 3", "type 4", "type 5",
        "type 1", "type 2", "type 3", "type 4", "type 5")
        
# convert to factors
group <- factor(lbls, levels=c("type 1", "type 2", "type 3", "type 4", "type 5"))
        
# create a dataframe with obs and group (optional)
data <- data.frame(obs, group, stringsAsFactors=TRUE)

# kruskal wallis test
res <- kruskal.test(obs, group)

# compute the critical point of the chi-square distribution X_{0.95, 4}
cp = qchisq(0.95, df=4);

# display info
cat(sprintf("p-value:         %.4f\n", res$p.value));
cat(sprintf("H*-statistic:    %.4f\n", res$statistic));
cat(sprintf("critical point:  %.4f\n", cp));


# post hoc test
#        type 1  type 2  type 3  type 4 
# type 2 1.00000 -       -       -      
# type 3 0.00376 0.00188 -       -      
# type 4 0.00103 0.00052 1.00000 -      
# type 5 1.00000 1.00000 0.00488 0.00133
res = posthoc.kruskal.conover.test(x=obs, g=group, p.adjust.method="bonferroni")
