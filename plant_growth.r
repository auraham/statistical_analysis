# plant_growth.r

# uncomment this line to install this package
# it is required for the dunn function
# install.packages("PMCMR")
# install.packages("PMCMRplus")

# remove all
rm(list=ls())

require(stats)
require(PMCMR)
data(PlantGrowth)

weight = PlantGrowth$weight
group = PlantGrowth$group


# kruskal wallis test
res <- kruskal.test(weight, group)

# compute the critical point of the chi-square distribution X_{0.95, 4}
#cp = qchisq(0.95, df=4);

# display info
#cat(sprintf("p-value:         %.4f\n", res$p.value));
#cat(sprintf("H*-statistic:    %.4f\n", res$statistic));
#cat(sprintf("critical point:  %.4f\n", cp));


# here, we use the bonferroni-type adjustment of p-values
# to alleviate the type I error
#posthoc.kruskal.dunn.test(x=count, g=spray, p.adjust.method="bonferroni")


