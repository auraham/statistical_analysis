# Statistical analysis

This repository contains my notes about conducting a statistical analysis for comparison of algorithms.

The aim of these notes is to show you how to conduct a statistical analysis for determining whether an algorithm A is better than another algorithm B, C, D, etc. If so, we can use these tests to support such an improvement, and 

In this case, we will use a non-parametric test called Kruskal-Wallis. Unlike other tests, like the parametric ANOVA, Kruskal-Wallis makes a few assumptions regarding the data.

I decided to make these notes to help others to conduct a statistical test. This guide is by no means a comprehensive study of the different test avaliable for this purpose. Instead, this guide provides a systematic/organized review  considering a few examples from different resources.



**Our approach**

 In general, we will follow this approach:

1. Given `C` samples (i.e., groups or algorithms), denoted as A, B, C, etc., with `N` observations in total, we want to determine whether there is a difference in these groups. To this end, we use the Kruskal-Wallis test.
2. The Kruskal-Wallis test, like many other statistical tests, considers two hypothesis:
   1. Null hypothesis ($H_0$) The medians of the samples are the same. In other words, this hypothesis states that all the considered samples (algorithms in our context) behave similarly, i.e., any of them is better that another one.
   2. Alternative hypothesis ($H_1$) At least one of the medians of the samples is different. In other words, at least one algorithm behaves different from the others. This means that one of them could be better or worse than the rest. Notice that if the alternative hypothesis is accepted, all we know is that the samples (algorithms) are different/ there is a difference in the samples (algorithms). Although, at this point, there is no way which of them is the best. To this end, we need to conduct something called a **post-hoc procedure**.
3. Conduct a post-hoc procedure. This procedure is performed only if the Kruskal-Wallis test concluded that there is a significant difference among samples (algorithms). In this procedure, we conduct a pair-wise comparison of the samples in order to determine whether sample (algorithm) A is (statistically) similar to sample B. Alternatively, we can choose a control method, say A, as a baseline. In this case, we compare the control method against the rest to determine whether A is statistically different or similar to sample B, sample C, sample D, etc. There are different post-hoc procedures, like Dunn and Conovar. In addition to the post-hoc procedure, some authors recommend to *correct* the outcome of the tests (something called *p-value*) to prevent errors related to/resulting from comparing samples multiple times. There are many of such methods, like Bonferroni and Dunn-Sidak.

In my opinion, one of the main challenges of conducting a statistical test is to determine a well-defined procedure and understand each step of it. As far as I know, there is no a standard approach for this end. You may find papers with statements like this:

> [...] each algorithm is run for 20 times on each test problem independently, and the Wilcoxon rank sum is adopted to compare the results obtained by RVEA (control method) and those by five compare algorithms at a significance level of 0.05 ($\alpha=0.05$). Symbol "+" indicates that the compared algorithm is significantly outperformed by (inferior than) RVEA according to a Wilcoxon rank sum test, while "-" means that RVEA is significantly outperformed by (inferior than) the compared algorithm. Finally, "~" means that there is no statistically significant difference between the results obtained by RVEA and the compared algorithm [Cheng16]

Throughout this guide, we will use an approach similar to that employed in [Cheng16] (see **Our approach** above). Instead of using the Wilcoxon rank sum test, we will use the Kruskal-Wallis test. If there is a significant difference among the samples regarding the Kruskal-Wallis test, then we use the Dunn test with Bonferroni correction as post-hoc procedure. In this procedure, we will select a sample (algorithm) as a control method. Then, we will compare the control method against the rest of the samples (algorithms) to determine whether the control method is better (+), similar (~), or inferior (-) than the compared sample (algorithm).



----

**Note** You may be aware that some authors recommend to perform another preliminary tests to determine whether the observations are normal (like normality test, hetero... and so on). These tests are a requirement for parametric tests like ANOVA (Kruskal-Wallis is the analog test to ANOVA [cite]). However, non-parametric tests like Kruskal-Wallis does not assume that the observations are normal. In fact, Kruskal-Wallis makes only a few assumptions of the observations [cite]. For this reason, Kruskal-Wallis is adopted in this guide, since we do not assume that the observations are normal.

----



The rest of the guide is organized as follows. 

- **Nomenclature** We use the conventions given in [Chan97].
- **Toy-example: types of coating** Here, we review a toy-example from [Ostertagova14] to understand the steps in the Kruskal-Wallis tests.
- **A real example** We compare the performance of several algorithms against a control method.
- **Installation of software** This final section describes how to install the R software needed for this guide.



If you find any issue or have a comment to improve the quality of this guide, please let me know. The source code is available in [github](https://gitlab.com/auraham/statistical_analysis).



## Nomenclature

The following table contains the nomenclature employed in this guide. This table is reproduced from [Chen97].

| Term | Abbreviation |
| ---- | ------------ |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |
|      |              |



## Toy-example: types of coating

We explain how to use the Kruskal-Wallis test, as described in [Ostertagova14]. We are interested in the effect of conductivity regarding five types of coating. Using a significance level of alpha = 0.05, is there a difference due to the coating type?



### Description

In [Ostertagova14]:

> Using the formulas (4), (6), and (7) we obtained:
>
> - the Kruskal-Wallis H-statistic, H=17.2412,
> - the correction factor for ties, f*=0.9977,
> - the corrected value of the H-statistic for ties, H*=17.2811
>
> Since the obtained H* value was larger than the critical point of the chi-square distribution X_{0.95,4} = 9.4877, the null hypothesis H_0 was rejected on significance level 0.05.




### Matlab

```matlab
% coating.m

% Each column represents a different coating type
X = [143 150 134 129 147;
     141 149 133 127 148;
     150 137 132 132 144;
     146 134 127 129 142;
     145 152 128 130 143];

showplot = 'on';
[p, tbl, stats] = kruskalwallis(X, [], showplot);
```

-----

**Note** Alternatively, we can provide the label of each sample as follows:

```matlab
X = [143 150 134 129 147 ...
     141 149 133 127 148 ...
     150 137 132 132 144 ...
     146 134 127 129 142 ...
     145 152 128 130 143];

lbls = {'type 1' 'type 2' 'type 3' 'type 4' 'type 5' ...
        'type 1' 'type 2' 'type 3' 'type 4' 'type 5' ...
        'type 1' 'type 2' 'type 3' 'type 4' 'type 5' ...
        'type 1' 'type 2' 'type 3' 'type 4' 'type 5' ...
        'type 1' 'type 2' 'type 3' 'type 4' 'type 5'};

showplot = 'on';
[p, tbl, stats] = kruskalwallis(X, lbls, 'on');
```

-----

The `kruskalwallis`  function will generate the following figure (the labels in the x-axis appear if you use `lbls` when calling the function): 

![](boxplots.png)

Also, you will see this table:

![](kruskalwallis.png)



`[p, tbl, stats] = kruskalwallis(X, [], displayopt)`  returns these variables:

- `p` is the p-value for the for the null hypothesis that the data in each column of the matrix `X` comes from the same distribution. The alternative hypothesis is that not all samples come from the same distribution. `kruskalwallis` also returns an ANOVA table (see figure above, labeled as *Kruskal-Wallis ANOVA Table*) and a box plot (see first figure above).
- `tbl` is the ANOVA table as a cell array.
- `stats` is a `struct` that contains information about the test statistics. In this case, it contains the mean of the ranks of each group.

To compute the value of the chi-square distribution X_{0.95,4} in matlab, use this command:

```matlab
% compute the critical point of the chi-square distribution X_{0.95, 4}
cp = chi2inv(0.95, 4);
```

At the end of the script, you will see this output:

```
>> coating
p-value:         0.0017
H*-statistic:    17.2811
critical point:  9.4877
```

These values are the same as those reported in  [Ostertagova14]. Thus, we can conclude that our implementation is correct.

----

**Note** You may notice that we do not perform a post hoc test here. This is becasue, as far as we know, the Conovar test is not available in matlab. Instead, you may choose from other options, like Dunn-Sidak or Bonferroni. For example:

```matlab
c = multcompare(stats, 'alpha', 0.05, 'ctype', 'bonferroni');
```

This is the content of `c`:


```matlab
c =

    1.0000    2.0000  -13.8509   -0.8000   12.2509    1.0000
    1.0000    3.0000   -1.5509   11.5000   24.5509    0.1338
    1.0000    4.0000   -0.0509   13.0000   26.0509    0.0517
    1.0000    5.0000  -12.7509    0.3000   13.3509    1.0000
    2.0000    3.0000   -0.7509   12.3000   25.3509    0.0816
    2.0000    4.0000    0.7491   13.8000   26.8509    0.0300
    2.0000    5.0000  -11.9509    1.1000   14.1509    1.0000
    3.0000    4.0000  -11.5509    1.5000   14.5509    1.0000
    3.0000    5.0000  -24.2509  -11.2000    1.8509    0.1600
    4.0000    5.0000  -25.7509  -12.7000    0.3509    0.0630
```

According to the [documentation](https://www.mathworks.com/help/stats/multcompare.html), the meaning of the column is as follows:

- The first two columns show the groups that are compared. For instance, `2.0000    3.0000` means that `type 2` and `type 3` are compared.
- The fourth column shows the difference between the estimated group means.
- The third and fifth columns show the lower and upper limits for 95% confidence intervals for the true mean difference.
- The sixth column contains the p-values for a hypothesis test that the corresponding mean difference is equal to zero.

You can find this example in the same documentation:

```matlab
c = 3×6

    1.0000    2.0000    0.9260    1.5000    2.0740    0.0000
    1.0000    3.0000    1.6760    2.2500    2.8240    0.0000
    2.0000    3.0000    0.1760    0.7500    1.3240    0.0116
```

Here, all p-values (0, 0.116) are very small, which indicates that there is a difference among the groups.

Back to our first `c` matrix:

```matlab
c =

    1.0000    2.0000  -13.8509   -0.8000   12.2509    1.0000
    1.0000    3.0000   -1.5509   11.5000   24.5509    0.1338
    1.0000    4.0000   -0.0509   13.0000   26.0509    0.0517
    1.0000    5.0000  -12.7509    0.3000   13.3509    1.0000
    2.0000    3.0000   -0.7509   12.3000   25.3509    0.0816
    2.0000    4.0000    0.7491   13.8000   26.8509    0.0300
    2.0000    5.0000  -11.9509    1.1000   14.1509    1.0000
    3.0000    4.0000  -11.5509    1.5000   14.5509    1.0000
    3.0000    5.0000  -24.2509  -11.2000    1.8509    0.1600
    4.0000    5.0000  -25.7509  -12.7000    0.3509    0.0630
```

It seems that any of the p-values is smaller than 0.05. That is, all the coating types are similar, even when there is a difference regarding `kruskalwallis`. Similar results were obtained when using other methods:

```matlab
multcompare(stats, 'alpha', 0.05, 'display', 'off', 'ctype', 'bonferroni');
multcompare(stats, 'alpha', 0.05, 'display', 'off', 'ctype', 'dunn-sidak');
multcompare(stats, 'alpha', 0.05, 'display', 'off', 'ctype', 'tukey-kramer');
multcompare(stats, 'alpha', 0.05, 'display', 'off', 'ctype', 'scheffe');
```

In [Ostertagova14], the authors used a method called Conover-Inman to compare each group (coating type), although they did not mention any correction method (like Bonferroni). Their results show that the following groups are different:

| Pairs i, j |
| ---------- |
| 1, 3       |
| 1, 4       |
| 2, 3       |
| 2, 4       |
| 3, 5       |
| 4, 5       |

For instance, `type 1` is different to `type 3`; `type 2` is different to `type 4`, and so on. In this guide, we aim to get these results in R and python. 

----




### R

Now, we perform the same test in R using ` kruskal.test`:

```R
# coating.r

# observations
obs <- c(143, 150, 134, 129, 147,
        141, 149, 133, 127, 148,
        150, 137, 132, 132, 144,
        146, 134, 127, 129, 142,
        145, 152, 128, 130, 143)

# labels
lbls <- c("type 1", "type 2", "type 3", "type 4", "type 5",
        "type 1", "type 2", "type 3", "type 4", "type 5",
        "type 1", "type 2", "type 3", "type 4", "type 5",
        "type 1", "type 2", "type 3", "type 4", "type 5",
        "type 1", "type 2", "type 3", "type 4", "type 5")
        
# convert to factors
group <- factor(lbls, levels=c("type 1", "type 2", "type 3", "type 4", "type 5"))

# kruskal wallis test
res <- kruskal.test(obs, group)
```

To compute the value of the chi-square distribution X_{0.95,4} in R, use this command:

```R
# compute the critical point of the chi-square distribution X_{0.95, 4}
cp = qchisq(0.95, df=4);
```

This is the output:

```
> res

	Kruskal-Wallis rank sum test

data:  obs and group
Kruskal-Wallis chi-squared = 17.281, df = 4, p-value = 0.001704
```

You will see this output at the end of the script:

```R
> source('coating.r')
p-value:         0.0017
H*-statistic:    17.2811
critical point:  9.4877
```

As it can be seen, it is the same output as that obtained in matlab. We can conclude that this implementation is correct.

Finally, we evaluate the post-hoc procedure as recommended in [Pohlert19]:

```R
# post hoc test
res = posthoc.kruskal.conover.test(x=obs, g=group, p.adjust.method="bonferroni")
```

This is the output:

```
> res

	Pairwise comparisons using Conover's-test for multiple	
                         comparisons of independent samples 

data:  obs and group 

       type 1  type 2  type 3  type 4 
type 2 1.00000 -       -       -      
type 3 0.00376 0.00188 -       -      
type 4 0.00103 0.00052 1.00000 -      
type 5 1.00000 1.00000 0.00488 0.00133

P value adjustment method: bonferroni 
```



### Python

Finally, we show how to conduct the statistical test in python. To this end, we first need to define the values and their labels as follows:

```python
# observations
# each column represents a different coating type
X = np.array([[143, 150, 134, 129, 147],
            [141, 149, 133, 127, 148],
            [150, 137, 132, 132, 144],
            [146, 134, 127, 129, 142],
            [145, 152, 128, 130, 143]])

# flatten observations
obs = X.flatten()

# labels
lbls = ('type 1', 'type 2', 'type 3', 'type 4', 'type 5',
        'type 1', 'type 2', 'type 3', 'type 4', 'type 5',
        'type 1', 'type 2', 'type 3', 'type 4', 'type 5',
        'type 1', 'type 2', 'type 3', 'type 4', 'type 5',
        'type 1', 'type 2', 'type 3', 'type 4', 'type 5');

# kruskal wallis test
H_value, p_value = ss.kruskal(X[:, 0], X[:, 1], X[:, 2], X[:, 3], X[:, 4])

# compute the critical point of the chi-square distribution X_{0.95, 4}
cp = chi2.isf(q=0.05, df=4)     # inverse survival function

# display info
print("p-value:         %.4f" % p_value);
print("H*-statistic:    %.4f" % H_value);
print("critical point:  %.4f" % cp);
print("")

# post hoc test
res = sp.posthoc_conover(df2, val_col="obs", group_col="lbls", p_adjust="bonferroni")

# print results
print_posthoc_result(res)
```

Also, we must convert the same data as a `pd.DataFrame` for the post hoc test:

```python
# create dataframe
data = {"obs": obs, "lbls": lbls}
df2 = pd.DataFrame(data, columns=["obs", "lbls"])
```

Now, we can evaluate the Kruskal Wallis test. Notice that we give each column as input data:

```python
# kruskal wallis test
H_value, p_value = ss.kruskal(X[:, 0], X[:, 1], X[:, 2], X[:, 3], X[:, 4])
```

To compute the value of the chi-square distribution X_{0.95,4} in python, use this command:

```python
# compute the critical point of the chi-square distribution X_{0.95, 4}
cp = chi2.isf(q=0.05, df=4)     # inverse survival function
```

Like the previous example, we use a post hoc procedure called Conovar with Bonferroni as correction method:

```python
# post hoc test
res = sp.posthoc_conover(df2, val_col="obs", group_col="lbls", p_adjust="bonferroni")
```

You can run the script as follows:

```
python3 coating.py 

p-value:         0.0017
H*-statistic:    17.2811
critical point:  9.4877

        type 1  type 2  type 3  type 4  
type 2  1.00000 -       -       -       
type 3  0.00376 0.00188 -       -       
type 4  0.00103 0.00052 1.00000 -       
type 5  1.00000 1.00000 0.00488 0.00133 
```

Notice that we achieved the same output as that obtained in the previous example in R.





**todo**

- [ ] crea ejemplo real con los resultados de mss
- [ ] crea un script para hacer las prubeas estadisticas y tablas (para integrarlo a rocket y reutilizarlo)



```
        type 1  type 2  type 3  type 4        
type 5  1.00000 1.00000 0.00488 0.00133



          type 1    type 2    type 3    type 4    type 5
type 1 -1.000000  1.000000  0.003764  0.001031  1.000000
type 2  1.000000 -1.000000  0.001883  0.000521  1.000000
type 3  0.003764  0.001883 -1.000000  1.000000  0.004883
type 4  0.001031  0.000521  1.000000 -1.000000  0.001334
type 5  1.000000  1.000000  0.004883  0.001334 -1.000000
```









## Real example

In this section, we show how to use the functions described above to conduct a statistical comparison of the performance of seven algorithms. Our task is to determine if the algorithms are different regarding their individual performance. If so (i.e., at least one of them is different), we then conduct a post hoc procedure to compare pairs of algorithms.



### Data

The data is available in `hv_dtlz1_m_objs_3_baselines_case_a.csv`. This file contains seven columns, one for each algorithm. It contains a matrix of shape `(50, 7)`. Each column represents the performance of an algorithm over 50 independent runs regarding the hypervolume indicator. Higher hypervolume values are preferred.

The first row of the csv file contains the names of the algorithms:

| Algorithm                                                    | Short name |
| ------------------------------------------------------------ | ---------- |
| `moead_norm_v1-ws-norm-true`                                 | `ws`       |
| `moead_norm_v1-te-norm-true`                                 | `te`       |
| `moead_norm_v1-asf-norm-true`                                | `asf`      |
| `moead_norm_v1-pbi-norm-true`                                | `pbi`      |
| `moead_norm_v1-ipbi-norm-true`                               | `ipbi`     |
| `moead_norm_v1-vads-norm-true`                               | `vads`     |
| `mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true` | `mss`      |

From these methods, we are interested on determining whether `mss` is better (+), similar (~), or inferior (-) to the other methods.



### R

We first load the libraries and data:

```R
require(stats)
require(PMCMR)

# load data
file <- "hv_dtlz1_m_objs_3_baselines_case_a.csv"
data <- read.csv(file, header=TRUE, sep=";")
```

You can access each column using `data$colname` or `data[, colname]`:

```
> data[, "moead_norm_v1.te.norm.true"]
 [1] 0.9342356 0.9388503 0.9340877 0.9380540 0.9284480 0.9353423 0.9309601
 [8] 0.9288110 0.9319758 0.9308143 0.9349859 0.0000000 0.9357287 0.9340830
[15] 0.9303496 0.9330304 0.9346745 0.9337040 0.9332300 0.9332775 0.9257151
[22] 0.9348396 0.9338871 0.9349344 0.9368462 0.9336361 0.9362734 0.9269873
[29] 0.9225583 0.9376530 0.0000000 0.9330399 0.9373651 0.9285265 0.9329447
[36] 0.9307702 0.9345130 0.9321008 0.9303499 0.9329772 0.9383280 0.9354107
[43] 0.9289468 0.9356296 0.9347222 0.9283902 0.9345324 0.9338106 0.9378902
[50] 0.9367522
```

```
> data$moead_norm_v1.te.norm.true
 [1] 0.9342356 0.9388503 0.9340877 0.9380540 0.9284480 0.9353423 0.9309601
 [8] 0.9288110 0.9319758 0.9308143 0.9349859 0.0000000 0.9357287 0.9340830
[15] 0.9303496 0.9330304 0.9346745 0.9337040 0.9332300 0.9332775 0.9257151
[22] 0.9348396 0.9338871 0.9349344 0.9368462 0.9336361 0.9362734 0.9269873
[29] 0.9225583 0.9376530 0.0000000 0.9330399 0.9373651 0.9285265 0.9329447
[36] 0.9307702 0.9345130 0.9321008 0.9303499 0.9329772 0.9383280 0.9354107
[43] 0.9289468 0.9356296 0.9347222 0.9283902 0.9345324 0.9338106 0.9378902
[50] 0.9367522
```

We then extract each column as a new variable:

```R
# split data into groups
ws = data$X..moead_norm_v1.ws.norm.true
te = data$moead_norm_v1.te.norm.true
asf = data$moead_norm_v1.asf.norm.true
pbi = data$moead_norm_v1.pbi.norm.true
ipbi = data$moead_norm_v1.ipbi.norm.true
vads = data$moead_norm_v1.vads.norm.true
mss = data$mss.dci_norm_hyp_mean.divs.8.25.mean.10.ps.normal.ss.gra.norm.true
```

Next, create the labels of the observations:

```R
# create labels
lbl_ws = rep(c("ws"), times=50)      # "ws" "ws" ... "ws" 50 times, one for each observation
lbl_te = rep(c("te"), times=50)
lbl_asf = rep(c("asf"), times=50)
lbl_pbi = rep(c("pbi"), times=50)
lbl_ipbi = rep(c("ipbi"), times=50)
lbl_vads = rep(c("vads"), times=50)
lbl_mss = rep(c("mss"), times=50)
```

Concat the columns (observations) and labels. The labels are then converted into factors:

```R
# create observations
obs  = c(ws, te, asf, pbi, ipbi, vads, mss)
lbls = c(lbl_ws, lbl_te, lbl_asf, lbl_pbi, lbl_ipbi, lbl_vads, lbl_mss)

# convert to factors
group <- factor(lbls, levels=c("ws", "te", "asf", "pbi", "ipbi", "vads", "mss"))
```

Finally, we can create a dataframe with the observations and labels:

```R
# create a dataframe with obs and group (optional)
data <- data.frame(obs, group, stringsAsFactors=TRUE)
```

This dataframe contains the data stored in the csv file but it is organized into two colums, `obs` and `group`:

```
> data[1:5, ]
         obs group
1 0.30955588    ws
2 0.26808825    ws
3 0.09384192    ws
4 0.14690488    ws
5 0.37876432    ws

> data[345:350, ]
          obs group
345 0.9869427   mss
346 0.9908506   mss
347 0.9896329   mss
348 0.9952408   mss
349 0.9927153   mss
350 0.9786225   mss
```

Now, we can perform the Kruskal Wallis test:

```R
# kruskal wallis test
res <- kruskal.test(obs, group)
```

and the post hoc test:

```R
# post hoc test
res = posthoc.kruskal.conover.test(x=obs, g=group, p.adjust.method="bonferroni")
```

This is the output of the script:

```
p-value:         0.0000
H*-statistic:    240.2458
critical point:  9.4877
```

Since `H*>critical point` (`p-value < 0.05`), there is a difference among the algorithms.

The following table shows the p-values with respect to the post hoc test:

```
> source('moea.r')

> res

	Pairwise comparisons using Conover's-test for multiple	
                         comparisons of independent samples 

data:  obs and group 

     ws      te      asf     pbi     ipbi    vads  
te   1.7e-08 -       -       -       -       -     
asf  < 2e-16 6.9e-12 -       -       -       -     
pbi  1.4e-07 1.0000  5.7e-13 -       -       -     
ipbi 0.0042  < 2e-16 < 2e-16 < 2e-16 -       -     
vads < 2e-16 3.0e-12 1.0000  2.4e-13 < 2e-16 -     
mss  < 2e-16 < 2e-16 0.0014  < 2e-16 < 2e-16 0.0023
```

We can show the same information more clearly:

```
> res$p.value < 0.05
       ws    te   asf  pbi ipbi vads
te   TRUE    NA    NA   NA   NA   NA
asf  TRUE  TRUE    NA   NA   NA   NA
pbi  TRUE FALSE  TRUE   NA   NA   NA
ipbi TRUE  TRUE  TRUE TRUE   NA   NA
vads TRUE  TRUE FALSE TRUE TRUE   NA
mss  TRUE  TRUE  TRUE TRUE TRUE TRUE
```

Notice the last row. The control method, `mss`, is statistically different to the rest of the algorithms.

Finally, we can show a boxplot:

```R
# optional: boxplot
png("boxplot_r.png")
boxplot(ws, te, asf, pbi, ipbi, vads, mss, names=c("ws", "te", "asf", "pbi", "ipbi", "vads", "mss"), ylab="Hypervolume")
dev.off()
```

![](boxplot_r.png)



### Python

In this section, we describe the procedure for conducting the statistical test as explained before. Some of the code is not shown for brevity.

We first load the data:

```python
filename = "hv_dtlz1_m_objs_3_baselines_case_a.csv"
df = pd.read_csv(filename, sep=";")
X = df.values
cols = df.columns
```

```
In [7]: cols                                             
Out[7]: 
Index(['# moead_norm_v1-ws-norm-true', 'moead_norm_v1-te-norm-true',
       'moead_norm_v1-asf-norm-true', 'moead_norm_v1-pbi-norm-true',
       'moead_norm_v1-ipbi-norm-true', 'moead_norm_v1-vads-norm-true',
       'mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true'],
      dtype='object')
```

Since the name of the columns are large, we use another list with shorter names:

```
cols = [short_names[item.replace("# ", "")] for item in cols]
```

```
In [9]: cols                                             
Out[9]: ['ws', 'te', 'asf', 'pbi', 'ipbi', 'vads', 'mss']
```

Next, we create a dataframe with the observations and labels. Here, `obs`

```python
# n_rows: number of observations of each group (algorithm)
# n_cols: number of groups (algorithms) in X
n_rows, n_cols = X.shape

# flatten observations
obs = X.flatten()

# assign labels
# we repeat [col_name_1, col_name_2, ..., col_name_l] 
# n_rows times, one for each row in X
lbls = cols * n_rows

# create dataframe
data = {"obs": obs, "lbls": lbls}
df = pd.DataFrame(data, columns=["obs", "lbls"])
```

Now, we are ready to use the Kruskal Wallis test:

```python
# kruskal wallis test
H_value, p_value = ss.kruskal(*[X[:, col] for col in range(n_cols)])

# the previous lines is the same as
# H_value, p_value = ss.kruskal(X[:, 0], X[:, 1], X[:, 2], X[:, 3], X[:, 4], X[:, 5], X[:, 6])
```

and the post hoc test:

```python
# post hoc test
posthoc_res = sp.posthoc_conover(df, val_col="obs", group_col="lbls", p_adjust="bonferroni")
```

This is the output of the script:

```
python3 moea.py

p-value:         0.0000
H*-statistic:    240.2458
critical point:  9.4877

        asf     ipbi    mss     pbi     te      vads    
ipbi    0.00000 -       -       -       -       -       
mss     0.00139 0.00000 -       -       -       -       
pbi     0.00000 0.00000 0.00000 -       -       -       
te      0.00000 0.00000 0.00000 1.00000 -       -       
vads    1.00000 0.00000 0.00232 0.00000 0.00000 -       
ws      0.00000 0.00415 0.00000 0.00000 0.00000 0.00000 

        asf     ipbi    pbi     te      vads    ws      
mss     0.00139 0.00000 0.00000 0.00000 0.00232 0.00000
```

First, notice that the value of `H*` obtained with R and python is the same. We also show the p-values of the post hoc test (although the order of columns and rows is different).

At the end, we can see the p-values of the control method `mss` with respect to the other methods. The block below contains the values obtained with R:

```
     ws      te      asf     pbi     ipbi    vads   
mss  < 2e-16 < 2e-16 0.0014  < 2e-16 < 2e-16 0.0023
```

Most of them are almost zero. The values corresponding to `asf` (`0.0014`) and `vads` (`0.0023`) are similar to the values obtained with python (`0.0014` was rounded). That is, both implementations produce the same output.



### Additional utilities

We can use the `get_posthoc_summary` function to get a summary of the results of the post hoc test:

```python
# optional: get post hoc summary
medians = df_hv2.median(axis=0)     # median of the hypervolume of each method
tab_summary, tab_pvalues = get_posthoc_summary(medians, posthoc_res, control="mss",
                                               min_is_better=False, alpha=0.05)
```

That function returns two dictionaries. The first one, `tab_summary`, contains the symbols `+`, `-`, `~` to indicate whether the control method is superior, inferior, or similar to the compared method (see the `posthoc_res` dataframe above):

```python
tab_summary =                                                          
{'mss-asf': '+',     # mss is superior than asf since median[mss] > median[asf]
 'mss-ipbi': '+',
 'mss-pbi': '+',
 'mss-te': '+',
 'mss-vads': '+',
 'mss-ws': '+'}
```

The second one,  `tab_pvalues`, contains the p-value corresponding to the control method and another method (see the `posthoc_res` dataframe above):

```python
tab_pvalues =
{'mss-asf': 0.00139439138385109,          # mss and asf are different since 0.00139 < alpha = 0.05
 'mss-ipbi': 5.2049797796112536e-65,
 'mss-pbi': 1.4069216388300212e-26,
 'mss-te': 3.2921729820505965e-25,
 'mss-vads': 0.0023218446556057455,
 'mss-ws': 6.770334833306552e-50}
```

We can also create a boxplot like this one:

```python
# optional: boxplot
# create a boxplot for each column in X
boxplot(X, cols, ylabel="Hypervolume")
```

![](boxplot_py.png)


Observe that the label of each boxplot contains the symbol `+` as indicated by `tab_summary`.



## Installation of software

Before going any further, install the required software.

**Install R**

```
sudo apt-get install r-base-core r-base
```

**Install R studio (optional)** from [here](https://rstudio.com/products/rstudio/download/#download).

```
wget https://download1.rstudio.org/desktop/xenial/amd64/rstudio-1.2.5033-amd64.deb
sudo dpkg -i rstudio-1.2.5033-amd64.deb
sudo apt-get -f install     # run this if you find any issue
```

**Install R packages** Run these commands from R Studio:

```
> install.packages("PMCMR")
> install.packages("PMCMRplus")
```

**Python packages**

```
pip3 install statsmodels
pip3 install scikit-posthocs
```



## References

- [Chan97] Learning and understanding the Kruskal-Wallis one-way analysis-of-variance-by-ranks test for differences among three of more independent groups.
- [Ostertagova14] Methodology and Application of the Kruskal-Wallis Test.
- [Cheng16] A Reference Vector Guided Evolutionary Algorithm for Many-Objective Optimization.
- [Pohlert19] The Pairwise Multiple Comparison of Mean Ranks Package (PMCMR).
- [Terpilowski20] scikit-posthocs Documentation, Release 0.6.1 

- [Install R and R studio](https://computingforgeeks.com/how-to-install-r-and-rstudio-on-ubuntu-debian-mint/)
- [Chi-square matlab](https://www.mathworks.com/help/stats/chi2inv.html)
- [Chi-square R](http://www.r-tutor.com/elementary-statistics/probability-distributions/chi-squared-distribution)
- [How to combine vectors in R](https://www.dummies.com/programming/r/how-to-combine-vectors-in-r/)
- [How to reshape data in R?](https://www.sixhat.net/how-to-reshape-data-in-r.html)
- [Import data into R](https://www.guru99.com/r-import-data.html)



## Contact

Auraham Camacho `auraham.cg@gmail.com`