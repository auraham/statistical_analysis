# moea.py
# We compare a few algorithms for solving a test problem.
# We also assess the performance of each algorithm regarding the Hypervolume indicator (higher values are preferred).
# Using a significance level of alpha = 0.05, is there a difference on the performance of the algorithms?
# 
# run
# python moea.py

import numpy as np
import pandas as pd
import scipy.stats as ss
import statsmodels.api as sa
import scikit_posthocs as sp
from scipy.stats import chi2
from statistical_analysis import print_posthoc_result
from statistical_analysis import get_posthoc_summary
from plot import boxplot

short_names = {
    "# moead_norm_v1-ws-norm-true": "ws",
    "moead_norm_v1-te-norm-true": "te",
    "moead_norm_v1-asf-norm-true": "asf",
    "moead_norm_v1-pbi-norm-true": "pbi",
    "moead_norm_v1-ipbi-norm-true": "ipbi",
    "moead_norm_v1-vads-norm-true" : "vads",
    "mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true": "mss",
}

if __name__ == "__main__":
    
    # load data
    # each column represents a different coating type
    filename = "hv_dtlz1_m_objs_3_baselines_case_a.csv"
    X = np.genfromtxt(filename, delimiter=";", comments="#", skip_header=True)

    # we can also use pandas
    # this way, we can get the name of each column
    df_hv = pd.read_csv(filename, sep=";")
    X = df_hv.values
    large_cols = df_hv.columns
    cols = [short_names[item] for item in large_cols]
    
    # optional: rename columns
    df_hv2 = df_hv.rename(short_names, axis="columns", inplace=False)
    
    # n_rows: number of observations of each group (algorithm)
    # n_cols: number of groups (algorithms) in X
    n_rows, n_cols = X.shape

    # flatten observations
    obs = X.flatten()
    
    # assign labels
    # we repeat [col_name_1, col_name_2, ..., col_name_l] 
    # n_rows times, one for each row in X
    lbls = cols * n_rows
    
    # create dataframe
    data = {"obs": obs, "lbls": lbls}
    df = pd.DataFrame(data, columns=["obs", "lbls"])

    # kruskal wallis test
    H_value, p_value = ss.kruskal(*[X[:, col] for col in range(n_cols)])
    
    # the previous lines is the same as
    # H_value, p_value = ss.kruskal(X[:, 0], X[:, 1], X[:, 2], X[:, 3], X[:, 4], X[:, 5], X[:, 6])
    
    # compute the critical point of the chi-square distribution X_{0.95, 4}
    cp = chi2.isf(q=0.05, df=4)     # inverse survival function

    # display info
    print("p-value:         %.4f" % p_value);
    print("H*-statistic:    %.4f" % H_value);
    print("critical point:  %.4f" % cp);
    print("")
    
    # post hoc test
    posthoc_res = sp.posthoc_conover(df, val_col="obs", group_col="lbls", p_adjust="bonferroni")

    # print results
    print_posthoc_result(posthoc_res); print("")
    print_posthoc_result(posthoc_res, control="mss")
    
    # optional: get post hoc summary
    medians = df_hv2.median(axis=0)     # median of the hypervolume of each method
    tab_summary, tab_pvalues = get_posthoc_summary(medians, posthoc_res, control="mss", min_is_better=False, alpha=0.05)

    # optional: boxplot
    # create a boxplot for each column in X
    boxplot(X, cols, tab_summary, control="mss", ylabel="Hypervolume", use_jitter=False)
    
