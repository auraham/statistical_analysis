# statistical_analysis.py
# Utilities for conducting a statistical analysis

import numpy as np
import pandas as pd
import scipy.stats as ss
import statsmodels.api as sa
import scikit_posthocs as sp
from scipy.stats import chi2

def print_posthoc_table(res):
    """
    Prints a table of the output of the post hoc procedure
    
    Example
    
            type 1  type 2  type 3  type 4  
    type 2  1.00000 -       -       -       
    type 3  0.00376 0.00188 -       -       
    type 4  0.00103 0.00052 1.00000 -       
    type 5  1.00000 1.00000 0.00488 0.00133
    """
    
    # it is a square matrix (dataframe)
    n, _ = res.shape
    data = res.values       # (n, n) np.array
    lbls = res.index        # Index(['type 1', 'type 2', 'type 3', 'type 4', 'type 5'], dtype='object')
    
    # print header
    title_len = np.max([7] + [len(lbl) for lbl in lbls]) + 1        # 7 is the length of a number, like 1.00000
    items = [" "] + [item for item in lbls[:n-1]]                   # last column is ignored
    fmt_items = [item.ljust(title_len) for item in items ]
    print("".join(fmt_items))
    
    for row in range(1, n):
        
        row_lbl = lbls[row].ljust(title_len)
        print(row_lbl, end="")
        
        for col in range(0, n-1):
            
            if col < row:
                value = data[row, col]
                str_value = "%.5f" % value
                print(str_value.ljust(title_len), end="")
            else:
                print("-".ljust(title_len), end="")
                
        print("")

def print_posthoc_control(res, control):
    """
    Prints the row of the output of the post hoc procedure
    regarding a control method
    
    Example (type 4 is the control method)
    
            type 1  type 2  type 3  type 5  
    type 4  0.00103 0.00052 1.00000 0.00133
    """
    
    # it is a square matrix (dataframe)
    n, _ = res.shape
    data = res.values       # (n, n) np.array
    lbls = res.index        # Index(['type 1', 'type 2', 'type 3', 'type 4', 'type 5'], dtype='object')
    
    # is control in lbls?
    if not control in lbls:
        print("control method (%s) is not in the list (%s)" % (control, lbls))
        return
        
    # filter labels
    lbls_fil = [item for item in lbls if item != control]
    
    # print header
    title_len = np.max([7] + [len(lbl) for lbl in lbls_fil]) + 1        # 7 is the length of a number, like 1.00000
    items = [" "] + [item for item in lbls_fil[:n-1]]                   # last column is ignored
    fmt_items = [item.ljust(title_len) for item in items ]
    print("".join(fmt_items))
    
    
    
    # row of interest
    # eg:
    #   res.loc[control]                                                                                                                                     
    #   type 1    0.001031
    #   type 2    0.000521
    #   type 3    1.000000
    #   type 4   -1.000000
    #   type 5    0.001334
    #   Name: type 4, dtype: float64
    values = res.loc[control]
    
    # row of interest (without control)
    # eg, type 4 is control:
    #   res.loc[control]                                                                                                                                     
    #   type 1    0.001031
    #   type 2    0.000521
    #   type 3    1.000000
    #   type 5    0.001334
    #   Name: type 4, dtype: float64
    values = res.loc[control, lbls_fil]
    
    # print row (without control)
    print(control.ljust(title_len), end="")
    print(" ".join(["%.5f" % val for val in values]))
    
def print_posthoc_result(res, control=None):
    
    if control:
        print_posthoc_control(res, control)
    else:
        print_posthoc_table(res)
    

# ---

def get_posthoc_summary(medians, posthoc_res, control="mss", min_is_better=False, alpha=0.05):
    """
    Return the summary of the post hoc test
    
    Example (type 4 is the control method):
    
            type 1  type 2  type 3  type 5  
    type 4  0.00103 0.00052 1.00000 0.00133
    
    We return a dictionary like this:
    
    tab_pvalues = {
        'type 4-type 1': 0.00103,       type 4 and type 1 are different since 0.00103 < alpha = 0.05
        'type 4-type 2': 0.00052,       type 4 and type 2 are different since 0.00052 < alpha = 0.05
        'type 4-type 3': 1.00000,       type 4 and type 3 are similar   since 1.00000 > alpha = 0.05
        'type 4-type 5': 0.00133,       type 4 and type 5 are different since 0.00133 < alpha = 0.05
    }
    
    and this (min_is_better=False)
    
    tab_summary = {
        'type 4-type 1': '+',           type 4 is superior than type 1 since median['type 4'] > median['type 1']
        'type 4-type 2': '-',           type 4 is inferior than type 2 since median['type 4'] < median['type 2']
        'type 4-type 3': '~',           type 4 is similar  to   type 3 since tab_pvalue['type 4-type 3'] > alpha = 0.05
        'type 4-type 5': '+',           type 4 is superior than type 5 since median['type 4'] > median['type 5']
    }
    """
    
    # it is a square matrix (dataframe)
    n, _ = posthoc_res.shape
    data = posthoc_res.values       # (n, n) np.array
    lbls = posthoc_res.index        # Index(['type 1', 'type 2', 'type 3', 'type 4', 'type 5'], dtype='object')
    
    # is control in lbls?
    if not control in lbls:
        print("control method (%s) is not in the list (%s)" % (control, lbls))
        return
        
    # filter labels
    lbls_fil = [item for item in lbls if item != control]
    
    # output
    tab_pvalues = {}
    tab_summary = {}
    
    for method in lbls_fil:
        
        dict_key = "%s-%s" % (control, method)          # key like 'type 4-type 1'
        p_value  = posthoc_res.loc[control, method]     # p-value
        conclusion = "~"                                # control and method are similar (by default)
        
        if p_value < alpha:
            
            # control and method are different
            if min_is_better:
                
                # control is superior (+) than method if medians[control] < medians[method]
                conclusion = "+" if medians[control] < medians[method] else "-"
            
            else:
                
                # control is superior (+) than method if medians[control] > medians[method]
                conclusion = "+" if medians[control] > medians[method] else "-"
        
        # update tables
        tab_pvalues[dict_key] = p_value
        tab_summary[dict_key] = conclusion
        
    return tab_summary, tab_pvalues
    
    
    
    
    
    
