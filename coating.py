# coating.py
# [Ostertagova14] Methodology and Application of the Kruskal-Wallis Test.
# We are interested in the effect of conductivity regarding five types of coating.
# Using a significance level of alpha = 0.05, is there a difference due to the coating type?
# 
# run
# python coating.py

import numpy as np
import pandas as pd
import scipy.stats as ss
import statsmodels.api as sa
import scikit_posthocs as sp
from scipy.stats import chi2

def print_posthoc_table(res):
    """
    Prints a table of the output of the post hoc procedure
    
    Example
    
            type 1  type 2  type 3  type 4  
    type 2  1.00000 -       -       -       
    type 3  0.00376 0.00188 -       -       
    type 4  0.00103 0.00052 1.00000 -       
    type 5  1.00000 1.00000 0.00488 0.00133
    """
    
    # it is a square matrix (dataframe)
    n, _ = res.shape
    data = res.values       # (n, n) np.array
    lbls = res.index        # Index(['type 1', 'type 2', 'type 3', 'type 4', 'type 5'], dtype='object')
    
    # print header
    title_len = np.max([7] + [len(lbl) for lbl in lbls]) + 1        # 7 is the length of a number, like 1.00000
    items = [" "] + [item for item in lbls[:n-1]]                   # last column is ignored
    fmt_items = [item.ljust(title_len) for item in items ]
    print("".join(fmt_items))
    
    for row in range(1, n):
        
        row_lbl = lbls[row].ljust(title_len)
        print(row_lbl, end="")
        
        for col in range(0, n-1):
            
            if col < row:
                value = data[row, col]
                str_value = "%.5f" % value
                print(str_value.ljust(title_len), end="")
            else:
                print("-".ljust(title_len), end="")
                
        print("")

def print_posthoc_control(res, control):
    """
    Prints the row of the output of the post hoc procedure
    regarding a control method
    
    Example (type 4 is the control method)
    
            type 1  type 2  type 3  type 5  
    type 4  0.00103 0.00052 1.00000 0.00133
    """
    
    # it is a square matrix (dataframe)
    n, _ = res.shape
    data = res.values       # (n, n) np.array
    lbls = res.index        # Index(['type 1', 'type 2', 'type 3', 'type 4', 'type 5'], dtype='object')
    
    # is control in lbls?
    if not control in lbls:
        print("control method (%s) is not in the list (%s)" % (control, lbls))
        return
        
    # filter labels
    lbls_fil = [item for item in lbls if item != control]
    
    # print header
    title_len = np.max([7] + [len(lbl) for lbl in lbls_fil]) + 1        # 7 is the length of a number, like 1.00000
    items = [" "] + [item for item in lbls_fil[:n-1]]                   # last column is ignored
    fmt_items = [item.ljust(title_len) for item in items ]
    print("".join(fmt_items))
    
    
    
    # row of interest
    # eg:
    #   res.loc[control]                                                                                                                                     
    #   type 1    0.001031
    #   type 2    0.000521
    #   type 3    1.000000
    #   type 4   -1.000000
    #   type 5    0.001334
    #   Name: type 4, dtype: float64
    values = res.loc[control]
    
    # row of interest (without control)
    # eg, type 4 is control:
    #   res.loc[control]                                                                                                                                     
    #   type 1    0.001031
    #   type 2    0.000521
    #   type 3    1.000000
    #   type 5    0.001334
    #   Name: type 4, dtype: float64
    values = res.loc[control, lbls_fil]
    
    # print row (without control)
    print(control.ljust(title_len), end="")
    print(" ".join(["%.5f" % val for val in values]))
    
    

def print_posthoc_result(res, control=None):
    
    if control:
        print_posthoc_control(res, control)
    else:
        print_posthoc_table(res)
        
if __name__ == "__main__":

    # observations
    # each column represents a different coating type
    X = np.array([[143, 150, 134, 129, 147],
                [141, 149, 133, 127, 148],
                [150, 137, 132, 132, 144],
                [146, 134, 127, 129, 142],
                [145, 152, 128, 130, 143]])

    # flatten observations
    obs = X.flatten()

    # labels
    lbls = ('type 1', 'type 2', 'type 3', 'type 4', 'type 5',
            'type 1', 'type 2', 'type 3', 'type 4', 'type 5',
            'type 1', 'type 2', 'type 3', 'type 4', 'type 5',
            'type 1', 'type 2', 'type 3', 'type 4', 'type 5',
            'type 1', 'type 2', 'type 3', 'type 4', 'type 5');

    # create dataframe
    data = {"obs": obs, "lbls": lbls}
    df2 = pd.DataFrame(data, columns=["obs", "lbls"])

    # kruskal wallis test
    H_value, p_value = ss.kruskal(X[:, 0], X[:, 1], X[:, 2], X[:, 3], X[:, 4])
    
    # compute the critical point of the chi-square distribution X_{0.95, 4}
    cp = chi2.isf(q=0.05, df=4)     # inverse survival function

    # display info
    print("p-value:         %.4f" % p_value);
    print("H*-statistic:    %.4f" % H_value);
    print("critical point:  %.4f" % cp);
    print("")

    # post hoc test
    res = sp.posthoc_conover(df2, val_col="obs", group_col="lbls", p_adjust="bonferroni")

    # print results
    print_posthoc_result(res)
    print_posthoc_result(res, control="type 4")



