# moea.r
# We compare a few algorithms for solving a test problem.
# We also assess the performance of each algorithm regarding the Hypervolume indicator (higher values are preferred).
# Using a significance level of alpha = 0.05, is there a difference on the performance of the algorithms?
# 
# run
# source('moea.r')

require(stats)
require(PMCMR)

# load data
file <- "hv_dtlz1_m_objs_3_baselines_case_a.csv"
csv_data <- read.csv(file, header=TRUE, sep=";")

# split data into groups
ws = csv_data$X..moead_norm_v1.ws.norm.true
te = csv_data$moead_norm_v1.te.norm.true
asf = csv_data$moead_norm_v1.asf.norm.true
pbi = csv_data$moead_norm_v1.pbi.norm.true
ipbi = csv_data$moead_norm_v1.ipbi.norm.true
vads = csv_data$moead_norm_v1.vads.norm.true
mss = csv_data$mss.dci_norm_hyp_mean.divs.8.25.mean.10.ps.normal.ss.gra.norm.true

# create labels
lbl_ws = rep(c("ws"), times=50)
lbl_te = rep(c("te"), times=50)
lbl_asf = rep(c("asf"), times=50)
lbl_pbi = rep(c("pbi"), times=50)
lbl_ipbi = rep(c("ipbi"), times=50)
lbl_vads = rep(c("vads"), times=50)
lbl_mss = rep(c("mss"), times=50)

# create observations
obs  = c(ws, te, asf, pbi, ipbi, vads, mss)
lbls = c(lbl_ws, lbl_te, lbl_asf, lbl_pbi, lbl_ipbi, lbl_vads, lbl_mss)

# convert to factors
group <- factor(lbls, levels=c("ws", "te", "asf", "pbi", "ipbi", "vads", "mss"))
        
# create a dataframe with obs and group (optional)
data <- data.frame(obs, group, stringsAsFactors=TRUE)

# kruskal wallis test
res <- kruskal.test(obs, group)

# compute the critical point of the chi-square distribution X_{0.95, 4}
cp = qchisq(0.95, df=4);

# display info
cat(sprintf("p-value:         %.4f\n", res$p.value));
cat(sprintf("H*-statistic:    %.4f\n", res$statistic));
cat(sprintf("critical point:  %.4f\n", cp));

# post hoc test
res = posthoc.kruskal.conover.test(x=obs, g=group, p.adjust.method="bonferroni")

# display control method
p_values_control = res$p.value["mss", ]
cat(sprintf("%.5f", p_values_control))

# optional: boxplot
png("boxplot_r.png")
boxplot(ws, te, asf, pbi, ipbi, vads, mss, names=c("ws", "te", "asf", "pbi", "ipbi", "vads", "mss"), ylab="Hypervolume")
dev.off()
